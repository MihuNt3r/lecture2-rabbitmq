﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using RabbitMQ;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                Console.WriteLine("Pinger");

                channel.QueueDeclare(queue: "ping_queue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($"{DateTime.Now} - {message}");

                    string responseMessage = "ping";
                    var responseBody = Encoding.UTF8.GetBytes(responseMessage);

                    Thread.Sleep(2500);

                    channel.BasicPublish(exchange: "",
                                     routingKey: "pong_queue",
                                     basicProperties: null,
                                     body: responseBody);
                };

                string startMessage = "ping";
                var startBody = Encoding.UTF8.GetBytes(startMessage);

                channel.BasicConsume(queue: "ping_queue",
                                     autoAck: true,
                                     consumer: consumer);

                channel.BasicPublish(exchange: "",
                                     routingKey: "pong_queue",
                                     basicProperties: null,
                                     body: startBody);

                Console.WriteLine(" [x] Sent {0}", startMessage);



                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }


            //Code which must perform same task with help of Wrapper class, but it doesn't work

            //Console.WriteLine("Pinger");

            //Wrapper pinger = new Wrapper();

            //pinger.ListenQueue("ping_queue");
            //pinger.SendFirstMessage("pong_queue", "ping");
            //pinger.SendMessageToQueue("pong_queue", "ping");

            //Console.WriteLine(" Press [enter] to exit.");
            //Console.ReadLine();

        }
    }
}
