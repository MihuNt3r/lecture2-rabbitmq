﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using RabbitMQ;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                Console.WriteLine("Ponger");

                channel.QueueDeclare(queue: "pong_queue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($"{DateTime.Now} - {message}");

                    string responseMessage = "pong";
                    var responseBody = Encoding.UTF8.GetBytes(responseMessage);

                    Thread.Sleep(2500);

                    channel.BasicPublish(exchange: "",
                                     routingKey: "ping_queue",
                                     basicProperties: null,
                                     body: responseBody);
                };

                channel.BasicConsume(queue: "pong_queue",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }

            //Code which must perform same task with help of Wrapper class, but it doesn't work

            //Console.WriteLine("Pinger");

            //Wrapper ponger = new Wrapper();

            //ponger.ListenQueue("pong_queue");
            //ponger.SendMessageToQueue("ping_queue", "pong");

            //Console.WriteLine(" Press [enter] to exit.");
            //Console.ReadLine();
        }
    }
}
