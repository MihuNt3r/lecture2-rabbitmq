﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ
{
    public class Wrapper
    {
        private IConnection _connection;
        private IModel _channel;
        private EventingBasicConsumer _consumer;


        public Wrapper()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _consumer = new EventingBasicConsumer(_channel);
        }

        public void ListenQueue(string queueName)
        {
            using(_connection)
            using (_channel)
            {
                _channel.QueueDeclare(queue: queueName,
                                      durable: false,
                                      exclusive: false,
                                      autoDelete: false,
                                      arguments: null);

                _consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine($"{DateTime.Now} - {message}");
                };

                _channel.BasicConsume(queue: queueName,
                                      autoAck: true,
                                      consumer: _consumer);
            }
        }

        public void SendMessageToQueue(string queueName, string message)
        {
            _consumer.Received += (model, ea) =>
            {
                var body = Encoding.UTF8.GetBytes(message);

                Thread.Sleep(2500);

                _channel.BasicPublish(exchange: "",
                                      routingKey: queueName,
                                      basicProperties: null,
                                      body: body);
            };
        }

        public void SendFirstMessage(string queueName, string message)
        {
            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "",
                                  routingKey: queueName,
                                  basicProperties: null,
                                  body: body);
        }
    }
}
